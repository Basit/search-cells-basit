import serial
import pandas as pd
from subprocess import run
import socket
output = run(['geni-get' , 'client_id'], capture_output=True).stdout.decode()
location = output.split("_")[1].split("\n")[0]
import sys
tty = "/dev/ttyUSB2"
import uhd
import numpy as np


def get_lines_to_OK(serial_port):
    buffer_lines = []
    buffer = b""
    while True:
        buffer += serial_port.read(1)
        if len(buffer)==0:
            continue
        # print(buffer)
        if buffer[-1] in b'\n\r':
            buffer_lines.append(buffer)
            if buffer.strip() == b"OK":
                break
            buffer = b""

    return buffer_lines

with serial.Serial(tty, timeout=0.1) as serial_port:

    serial_port.reset_input_buffer()
    serial_port.write(b"AT+QNWPREFCFG=\"nr5g_band\",1:2:3:5:7:8:12:13:14:18:20:25:26:28:29:30:38:40:41:48:66:70:71:75:76:77:78:79\r")

    lines = get_lines_to_OK(serial_port)
    print(lines)

    serial_port.reset_input_buffer()
    serial_port.write(b"AT+QNWPREFCFG=\"lte_band\",1:2:3:4:5:7:8:12:13:14:17:18:19:20:25:26:28:29:30:32:34:38:39:40:41:42:43:46:48:66:71\r")

    lines = get_lines_to_OK(serial_port)
    print(lines)