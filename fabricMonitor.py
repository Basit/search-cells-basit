from fabric import Connection, Config 
from fabric.tasks import task
import datetime
import time
import fabric
import threading
import time
import socket
import pandas as pd
import io
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart




from fabric import ThreadingGroup 

host_dict = {'bookstore': "BasitAw@nuc1.bookstore.powderwireless.net",'humanities': "BasitAw@nuc1.humanities.powderwireless.net", 'law73': "BasitAw@nuc1.law73.powderwireless.net", 'madsen': "BasitAw@nuc1.madsen.powderwireless.net",'ebc': "BasitAw@nuc1.ebc.powderwireless.net"}
# host_dict = {'bookstore': "BasitAw@nuc1.bookstore.powderwireless.net",'humanities': "BasitAw@nuc1.humanities.powderwireless.net", 'law73': "BasitAw@nuc1.law73.powderwireless.net", 'web': "BasitAw@nuc1.web.powderwireless.net", 'cpg': "BasitAw@nuc1.cpg.powderwireless.net", 'madsen': "BasitAw@nuc1.madsen.powderwireless.net",'ebc': "BasitAw@nuc1.ebc.powderwireless.net"}
# host_dict = {'bookstore': "BasitAw@nuc1.bookstore.powderwireless.net",'humanities': "BasitAw@nuc1.humanities.powderwireless.net",  'web': "BasitAw@nuc1.web.powderwireless.net", 'cpg': "BasitAw@nuc1.cpg.powderwireless.net", 'madsen': "BasitAw@nuc1.madsen.powderwireless.net"}
# host_dict = {'law73': "BasitAw@nuc1.law73.powderwireless.net",'madsen': "BasitAw@nuc1.madsen.powderwireless.net",'ebc': "BasitAw@nuc1.ebc.powderwireless.net"}
df_main = pd.DataFrame(columns=["Type","MCC", "MNC", "Freq", "PCI", "RSRP", "RSRQ", "srxlex", "squal/scs", "cellid", "TAC","Bandwidth","Band","Time","Location", "DLpower", "ULpower"])
host_list = list(host_dict.values())
# print("hello")
df_lock = threading.Lock()
start_time = datetime.datetime.now()

# Set the desired time interval (60 minutes)
interval = datetime.timedelta(minutes=60)    


spectrum_lock = threading.Lock()


spectrum_id = ""
spectrum_provided = False

import requests
import json


def provide():

    IDENTITY="https://node-0.rdz-dev.powderteam.emulab.net:8000"
    ZMC="https://node-0.rdz-dev.powderteam.emulab.net:8010"
    ELEMENT_ID="fa870f08-6be9-4f3f-bd74-81ee6e470c23"
    ELEMENT_TOKEN="rpu_Eo6vPGZhQWOKeSP2sjPCjhKGh5iKxWaAv7fUvfUDS2w3g3hugF"


    url = f"{ZMC}/v1/spectrum"

    # Define the payload as a Python dictionary
    payload = {
        "element_id": ELEMENT_ID,
        "name": "spectrum1",
        "description": "spectrum1",
        "url": "http://www.powderwireless.net/",
        "enabled": True,
        "starts_at": "2023-09-07T18:00:00.000000000Z",
        "constraints": [
            {
                "constraint": {
                    "min_freq": 2350000000,
                    "max_freq": 2360000000,
                    "max_eirp": -70,
                    "exclusive": True
                }
            }
        ],
        "policies": [
            {
                "element_id": ELEMENT_ID,
                "allowed": True,
                "auto_approve": True,
                "priority": 500
            }
        ]
    }

    # Define headers with the API token
    headers = {
        "X-Api-Token": ELEMENT_TOKEN,
        "X-Api-Elaborate": "true"
    }

    # Convert the payload to JSON format
    json_payload = json.dumps(payload)

    # Make the POST request
    try:
        response = requests.post(url, headers=headers, data=json_payload)
    except Exception as e:
        print(e)
        print("Something wrong with providing spectrum will try in 30 sec")
        time.sleep(30)
        return provide()

    # Print the response
    if response.status_code == 200:
        print("Provide Executed Properly")
        print("Spectrum Id ", response.json()["id"])
    else:
        print("Provide did not execute properly, trying again in 3o sec")
        time.sleep(30)
        return provide()
    print(response.status_code)
    print(response.text)
    print(response.json()["id"])
    return response.json()["id"]

def check(SPECTRUM_ID):
    ELEMENT_TOKEN="rpu_Eo6vPGZhQWOKeSP2sjPCjhKGh5iKxWaAv7fUvfUDS2w3g3hugF"
    ZMC="https://node-0.rdz-dev.powderteam.emulab.net:8010"
    # SPECTRUM_ID = "18b03514-4d23-41a1-9872-cdab0321903a"

    # Define the URL for the GET request
    url = f"{ZMC}/v1/spectrum/{SPECTRUM_ID}"

    # Define headers with the API token
    headers = {
        "X-Api-Token": ELEMENT_TOKEN,
        "X-Api-Elaborate": "true"
    }

    # Make the GET request
    response = requests.get(url, headers=headers)

    # Print the response
    print(response.status_code)
    if response.status_code == 200:
        print("Check Executed Properly")
    else:
        print("Check did not execute properly")
        return False
    # print(response.text)
    print(response.json())
    deleted = (response.json()["deleted_at"]== None)
    return not deleted

def delete(SPECTRUM_ID):
    ELEMENT_TOKEN="rpu_Eo6vPGZhQWOKeSP2sjPCjhKGh5iKxWaAv7fUvfUDS2w3g3hugF"
    ZMC="https://node-0.rdz-dev.powderteam.emulab.net:8010"
    # SPECTRUM_ID = "18b03514-4d23-41a1-9872-cdab0321903a"

    # Define the URL for the GET request
    url = f"{ZMC}/v1/spectrum/{SPECTRUM_ID}"

    # Define headers with the API token
    headers = {
        "X-Api-Token": ELEMENT_TOKEN,
        "X-Api-Elaborate": "true"
    }

    # Make the GET request
    response = requests.delete(url, headers=headers)

    # Print the response
    if response.status_code == 200:
        print("Delete Executed Properly")
    else:
        print("Delete did not execute properly, trying again")
        delete(id)
    print(response.status_code)


    id = SPECTRUM_ID
    if check(id):
        print("Deleted Sucessfully")
    else:
        print("Trying to Delete again")
        delete(id)
    
    # print(response.text)
    # print(response.json())



def monitor():
    while True:
        
        
        mo_start_time = datetime.datetime.now()
        time.sleep(300)
        spectrum_lock.acquire()
        global spectrum_provided
        spectrum_lock.release()
        if spectrum_provided == True:
            print("Spectrum is being provided")
            continue
        df_lock.acquire()
        global df_main
        check_df = df_main[df_main['Time']> mo_start_time]
        print("Df length is :", len(df_main))
        df_lock.release()
        check_df = check_df[(check_df['MCC']==310) & (check_df['MNC']==410) & (check_df["Freq"]==9820)].copy()
        check_df = check_df.loc[~(check_df['RSRP'] == '-')]
        if len(check_df) == 0:
            print("Monitor could not see any 9820 datapoints in last 5 mins")
            continue
        check_df['RSRP'] = check_df['RSRP'].astype(int)
        maxRSRP = check_df['RSRP'].max()
        print("At time ", mo_start_time, "and 5 mins after, the max RSRP was ", maxRSRP)
        if maxRSRP < -99:
            print("Going to be providing spectrum")
            
            # global spectrum_provided
            spectrum_id_1 = provide()
            spectrum_lock.acquire()
            global spectrum_id
            spectrum_id = spectrum_id_1
            spectrum_provided = True
            spectrum_lock.release()

    # id = provide()
    # print(check(id))
    # delete(id)
    # print(check(id))


def runFabric():
    failures = 0
    lastFailure = 0
    sLastFailure = 0
    counter = 0
    while True:
        threads = ThreadingGroup(*host_list)
        try:
            threads.run("sudo pkill -f runner.py")
        except KeyboardInterrupt:
            print("Ctrl-C detected. Exiting gracefully.")
            return
        except:
            pass
        
        try:
            dtmnorm = datetime.datetime.now()
            dtm = str(datetime.datetime.now())
            dtm = dtm.replace(' ','_')
            dtm = dtm.split('.')[0]
            # threads.run("sudo python3 runner.py %s" % dtm)
            print(dtm)
            print("Number of Failures = ", failures, lastFailure, sLastFailure, flush=True)
            threads.run("sudo python3 /local/repository/runner.py %s" % dtm, timeout=60)
            
            # df_lock.acquire()
            # global df_main
            # print("Df length is :", len(df_main))
            # df_lock.release()
            remaining =30 - (datetime.datetime.now() - dtmnorm).total_seconds()
            print("Remaing time is ", remaining)
            if remaining>0:
                time.sleep(remaining)
            threads.close()
            sLastFailure = lastFailure
            lastFailure = failures
        
        except Exception as e:
            sLastFailure = lastFailure
            lastFailure = failures 
            failures+=1
            print("Something went wrong, trying again")
            print(e)
            try:
                threads.run("sudo pkill -f runner.py")
            except:
                pass

            if failures%5==0:
                if failures - 1 == lastFailure and lastFailure - 1 == sLastFailure: 
                    print("Power Cycling all Nodes",flush=True)
                    try:
                        threads.run("ls /dev/tty*")
                        threads.run("node_reboot -f nuc1", timeout=30)
                    except:
                        pass
                    time.sleep(30)
            threads.close()
            
            time.sleep(10)
        

def handleCon(client_socket,client_address):
    dtmnorm = datetime.datetime.now()
    dtm = str(datetime.datetime.now())
    dtm = dtm.replace(' ','_')
    dtm = dtm.split('.')[0]
    connection_open = True
    received_data = b""  # Initialize an empty bytes object to store received data
    try:
        while connection_open:
            data_chunk = client_socket.recv(1024)  # Receive a chunk of data
            if not data_chunk:
                connection_open = False  # No more data received, exit the loop
            else:
                received_data += data_chunk  # Concatenate the received data chunks
    except:
        print("Connection was corrupted")
        return

    # Process the received data (replace with your own logic)
    # print(f"Received data from {client_address}: {received_data.decode('utf-8')}")

    # print(f"Connection with {client_address} closed.")
    client_socket.close()
    df_csv = received_data.decode('utf-8')
    received_df = pd.read_csv(io.StringIO(df_csv))
    try:
        location = received_df['Location'].unique()[0]
        time = received_df['Time'].unique()[0]
    except:
        print("Empty Df")
        return
    print(f"Written Df for {location} at time {time}")
    received_df['Time'] = pd.to_datetime(received_df['Time'], format='20%y-%m-%d_%H:%M:%S')
    received_df["EndTime"] = dtm
    received_df['EndTime'] = pd.to_datetime(received_df['EndTime'], format='20%y-%m-%d_%H:%M:%S')
    # spectrum_lock.acquire()
    global spectrum_provided
    # spectrum_lock.release()

    if spectrum_provided:
        received_df_checker = received_df[(received_df['MCC']==310) & (received_df['MNC']==410) & (received_df["Freq"]==9820)].copy()
        received_df_checker = received_df_checker.loc[~(received_df_checker['RSRP'] == '-')]

        if len(received_df) == 0:
            print("No 9820 datapoints") 
        else:
            received_df_checker['RSRP'] = received_df_checker['RSRP'].astype(int)
            maxRSRP = received_df_checker['RSRP'].max()
            if maxRSRP>-99:
                print(maxRSRP, " RSRP increased threshold going to delete the spectrum")
                global spectrum_id
                delete(spectrum_id)
                spectrum_lock.acquire()
                spectrum_provided = False
                spectrum_lock.release()

                




    received_df.to_csv("/local/www/"+location + "_"+time + '.csv')
    df_lock.acquire()
    global df_main
    global start_time
    current_time = datetime.datetime.now()
    df_main = pd.concat([df_main, received_df], ignore_index=True)
    print("Length of Df main is ", len(df_main))
    if current_time - start_time >= interval:
        df_main.to_csv("/local/www/"+str(current_time)+ 'main.csv')
        print("Df main has been written, size is = ,", len(df_main))
        df_main = df_main[df_main['Time']> start_time]
        print("Df main has been filtered, size is = ," ,len(df_main))
        # You can put your code to execute here
        start_time = current_time 
    df_lock.release()
def dfreceiver():
    # Create a server socket
    host = '155.98.38.44'
    port = 34500
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # Set the socket as reusable
    
    # Bind the socket to the host and port
    server_socket.bind((host, port))
    
    # Listen for incoming connections
    server_socket.listen(100)  # Maximum 100 queued connections

    print(f"Server listening on {host}:{port}")
    
    
    while True:
       
        # Accept a client connection
        client_socket, client_address = server_socket.accept()
        print(f"Connection established with {client_address}")
        # handleCon(client_socket,client_address)
        client_thread = threading.Thread(target=handleCon, args=(client_socket, client_address))
        client_thread.start()

        
        

# @task
def main():
   # threads = ThreadingGroup(*host_list)
   # threads.run("sh /local/repository/installer.sh" )

    # Start the thread
    recevier_thread = threading.Thread(target=dfreceiver)
    recevier_thread.start()
    # monitor_thread = threading.Thread(target=monitor)
    # monitor_thread.start()
    # Optionally, you can wait for the thread to complete
    runFabric()
    recevier_thread.join()
    # monitor_thread.join()
main()
