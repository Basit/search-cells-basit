from fabric import Connection, Config 
from fabric.tasks import task
import datetime
import time
import fabric
import threading
import time
import socket
import pandas as pd
import io
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart




from fabric import ThreadingGroup 

host_dict = {'bookstore': "BasitAw@nuc1.bookstore.powderwireless.net",'ebc': "BasitAw@nuc1.ebc.powderwireless.net", 'humanities': "BasitAw@nuc1.humanities.powderwireless.net", 'law73': "BasitAw@nuc1.law73.powderwireless.net", 'madsen': "BasitAw@nuc1.madsen.powderwireless.net", 'moran': 'BasitAw@nuc1.moran.powderwireless.net'}
# host_dict = {'bookstore': "BasitAw@nuc1.bookstore.powderwireless.net",'humanities': "BasitAw@nuc1.humanities.powderwireless.net", 'law73': "BasitAw@nuc1.law73.powderwireless.net", 'web': "BasitAw@nuc1.web.powderwireless.net", 'cpg': "BasitAw@nuc1.cpg.powderwireless.net", 'madsen': "BasitAw@nuc1.madsen.powderwireless.net",'ebc': "BasitAw@nuc1.ebc.powderwireless.net"}
# host_dict = {'bookstore': "BasitAw@nuc1.bookstore.powderwireless.net",'humanities': "BasitAw@nuc1.humanities.powderwireless.net",  'web': "BasitAw@nuc1.web.powderwireless.net", 'cpg': "BasitAw@nuc1.cpg.powderwireless.net", 'madsen': "BasitAw@nuc1.madsen.powderwireless.net"}
# host_dict = {'law73': "BasitAw@nuc1.law73.powderwireless.net",'madsen': "BasitAw@nuc1.madsen.powderwireless.net",'ebc': "BasitAw@nuc1.ebc.powderwireless.net"}
df_main = pd.DataFrame(columns=["Type","MCC", "MNC", "Freq", "PCI", "RSRP", "RSRQ", "srxlex", "squal/scs", "cellid", "TAC","Bandwidth","Band","Time","Location", "DLpower", "ULpower"])
host_list = list(host_dict.values())
# print("hello")
df_lock = threading.Lock()
start_time = datetime.datetime.now()

# Set the desired time interval (60 minutes)
interval = datetime.timedelta(minutes=60)    

def warningEmail():
    subject = "Alot of Failures Happened"
    message = "Detected alot of failures"
    global server

    try:
        msg = MIMEMultipart()
        msg['From'] = sender_email
        msg['To'] = receiver_email
        msg['Subject'] = subject
        msg.attach(MIMEText(message, 'plain'))
        server.sendmail(sender_email, receiver_email, msg.as_string())
    except:
        print("Server did not work properly")
        server.starttls()
        server.login(sender_email, password)
        msg = MIMEMultipart()
        msg['From'] = sender_email
        msg['To'] = receiver_email
        msg['Subject'] = subject
        msg.attach(MIMEText(message, 'plain'))
        server.sendmail(sender_email, receiver_email, msg.as_string())


def runFabric():
    failures = 0
    lastFailure = 0
    sLastFailure = 0
    counter = 0
    powercycled = False
    while True:
        threads = ThreadingGroup(*host_list)
        try:
            threads.run("sudo pkill -9 python3")
        except KeyboardInterrupt:
            print("Ctrl-C detected. Exiting gracefully.")
            return
        except:
            pass
        
        try:
            dtmnorm = datetime.datetime.now()
            dtm = str(datetime.datetime.now())
            dtm = dtm.replace(' ','_')
            dtm = dtm.split('.')[0]
            # threads.run("sudo python3 runner.py %s" % dtm)
            print(dtm)
            print("Number of Failures = ", failures, lastFailure, sLastFailure, flush=True)
            if powercycled:
                threads.run("cd /local/repository && git pull", timeout=30)
                powercycled = False
                
            threads.run("sudo python3 /local/repository/runner.py %s" % dtm, timeout=90)
            # threads.run("cd /local/repository && git pull", timeout=180)
            # threads.run("sudo python3 /local/repository/bandsetter.py", timeout=30)
            
            # df_lock.acquire()
            # global df_main
            # print("Df length is :", len(df_main))
            # df_lock.release()
            remaining =60 - (datetime.datetime.now() - dtmnorm).total_seconds()
            print("Remaing time is ", remaining)
            if remaining>0:
                time.sleep(remaining)
            threads.close()
            sLastFailure = lastFailure
            lastFailure = failures
        
        except Exception as e:
            sLastFailure = lastFailure
            lastFailure = failures 
            failures+=1
            print("Something went wrong, trying again")
            print(e)
            try:
                threads.run("sudo pkill -9 python3")
            except:
                pass

            if failures%5==0:
                if failures - 1 == lastFailure and lastFailure - 1 == sLastFailure: 
                    print("Power Cycling all Nodes",flush=True)
                    powercycled = True
                    try:
                        # threads.run("ls /dev/tty*")
                        threads.run("mkdir -p ~/.ssl ; chmod 750 ~/.ssl ; geni-get rpccert > ~/.ssl/emulab.pem", timeout=30)
                        threads.run("node_reboot -f nuc1", timeout=30)
                    except:
                        pass
                    time.sleep(30)
            threads.close()
            
            time.sleep(10)
        

def handleCon(client_socket,client_address):
    dtmnorm = datetime.datetime.now()
    dtm = str(datetime.datetime.now())
    dtm = dtm.replace(' ','_')
    dtm = dtm.split('.')[0]
    connection_open = True
    received_data = b""  # Initialize an empty bytes object to store received data
    try:
        while connection_open:
            data_chunk = client_socket.recv(1024)  # Receive a chunk of data
            if not data_chunk:
                connection_open = False  # No more data received, exit the loop
            else:
                received_data += data_chunk  # Concatenate the received data chunks
    except:
        print("Connection was corrupted")
        return

    # Process the received data (replace with your own logic)
    # print(f"Received data from {client_address}: {received_data.decode('utf-8')}")

    # print(f"Connection with {client_address} closed.")
    client_socket.close()
    df_csv = received_data.decode('utf-8')
    received_df = pd.read_csv(io.StringIO(df_csv))
    try:
        location = received_df['Location'].unique()[0]
        time = received_df['Time'].unique()[0]
    except:
        print("Empty Df")
        return
    print(f"Written Df for {location} at time {time}")
    received_df['Time'] = pd.to_datetime(received_df['Time'], format='20%y-%m-%d_%H:%M:%S')
    received_df["EndTime"] = dtm
    received_df['EndTime'] = pd.to_datetime(received_df['EndTime'], format='20%y-%m-%d_%H:%M:%S')

    received_df.to_csv("/local/www/"+location + "_" +str(time) + '.csv')
    df_lock.acquire()
    global df_main
    global start_time
    current_time = datetime.datetime.now()
    df_main = pd.concat([df_main, received_df], ignore_index=True)
    print("Length of Df main is ", len(df_main))
    if current_time - start_time >= interval:
        df_main.to_csv("/local/www/"+str(current_time)+ 'main.csv')
        print("Df main has been written, size is = ,", len(df_main))
        df_main = df_main[df_main['Time']> start_time]
        print("Df main has been filtered, size is = ," ,len(df_main))
        # You can put your code to execute here
        start_time = current_time 
    df_lock.release()
def dfreceiver():
    # Create a server socket
    host = '155.98.38.88'
    port = 34500
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # Set the socket as reusable
    
    # Bind the socket to the host and port
    server_socket.bind((host, port))
    
    # Listen for incoming connections
    server_socket.listen(100)  # Maximum 100 queued connections

    print(f"Server listening on {host}:{port}")
    
    
    while True:
       
        # Accept a client connection
        client_socket, client_address = server_socket.accept()
        print(f"Connection established with {client_address}")
        # handleCon(client_socket,client_address)
        client_thread = threading.Thread(target=handleCon, args=(client_socket, client_address))
        client_thread.start()

        
        

# @task
def main():
   # threads = ThreadingGroup(*host_list)
   # threads.run("sh /local/repository/installer.sh" )

    # Start the thread
    recevier_thread = threading.Thread(target=dfreceiver)
    recevier_thread.start()
    # Optionally, you can wait for the thread to complete
    runFabric()
    recevier_thread.join()
main()
