from fabric import Connection, Config 
from fabric.tasks import task
import datetime
import time
import fabric
import threading
import time
import socket
import pandas as pd
import io
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart




from fabric import ThreadingGroup 

host_dict = {'humanities': "BasitAw@nuc1.humanities.powderwireless.net", 'law73': "BasitAw@nuc1.law73.powderwireless.net", 'ebc': "BasitAw@nuc1.ebc.powderwireless.net", 'sagepoint': "BasitAw@nuc1.sagepoint.powderwireless.net", 'bes': "BasitAw@pc844.emulab.net", 'hospital': "BasitAw@pc797.emulab.net", 'fm': "BasitAw@pc10-fort.emulab.net"}
# host_dict = {'bookstore': "BasitAw@nuc1.bookstore.powderwireless.net",'humanities': "BasitAw@nuc1.humanities.powderwireless.net", 'law73': "BasitAw@nuc1.law73.powderwireless.net", 'web': "BasitAw@nuc1.web.powderwireless.net", 'cpg': "BasitAw@nuc1.cpg.powderwireless.net", 'madsen': "BasitAw@nuc1.madsen.powderwireless.net",'ebc': "BasitAw@nuc1.ebc.powderwireless.net"}
# host_dict = {'bookstore': "BasitAw@nuc1.bookstore.powderwireless.net",'humanities': "BasitAw@nuc1.humanities.powderwireless.net",  'web': "BasitAw@nuc1.web.powderwireless.net", 'cpg': "BasitAw@nuc1.cpg.powderwireless.net", 'madsen': "BasitAw@nuc1.madsen.powderwireless.net"}
# host_dict = {'law73': "BasitAw@nuc1.law73.powderwireless.net",'madsen': "BasitAw@nuc1.madsen.powderwireless.net",'ebc': "BasitAw@nuc1.ebc.powderwireless.net"}
df_main = pd.DataFrame(columns=["Type","MCC", "MNC", "Freq", "PCI", "RSRP", "RSRQ", "srxlex", "squal/scs", "cellid", "TAC","Bandwidth","Band","Time","Location", "DLpower", "ULpower"])
host_list = list(host_dict.values())
# print("hello")
df_lock = threading.Lock()
start_time = datetime.datetime.now()

# Set the desired time interval (60 minutes)
interval = datetime.timedelta(minutes=60)    

def runFabric():
    failures = 0
    lastFailure = 0
    sLastFailure = 0
    counter = 0
    while True:
        threads = ThreadingGroup(*host_list)
        try:
            threads.run("sudo pkill -9 FalconEye")
        except KeyboardInterrupt:
            print("Ctrl-C detected. Exiting gracefully.")
            return
        except:
            pass
        
        try:
            dtmnorm = datetime.datetime.now()
            dtm = str(datetime.datetime.now())
            dtm = dtm.replace(' ','_')
            dtm = dtm.split('.')[0]
            # threads.run("sudo python3 runner.py %s" % dtm)
            print(dtm)
            print("Number of Failures = ", failures, lastFailure, sLastFailure, flush=True)
            threads.run("sudo sh /local/repository/falconrunner.sh %s" % dtm, timeout=400)
            
            # df_lock.acquire()
            # global df_main
            # print("Df length is :", len(df_main))
            # df_lock.release()
            remaining =350 - (datetime.datetime.now() - dtmnorm).total_seconds()
            print("Remaing time is ", remaining)
            if remaining>0:
                time.sleep(remaining)
            threads.close()
            sLastFailure = lastFailure
            lastFailure = failures
        
        except Exception as e:
            sLastFailure = lastFailure
            lastFailure = failures 
            failures+=1
            print("Something went wrong, trying again")
            print(e)
            try:
                threads.run("sudo pkill -9 FalconEye")
            except:
                pass

            # if failures%5==0:
            #     if failures - 1 == lastFailure and lastFailure - 1 == sLastFailure: 
            #         print("Power Cycling all Nodes",flush=True)
            #         try:
            #             threads.run("ls /dev/tty*")
            #             threads.run("node_reboot -f nuc1", timeout=400)
            #         except:
            #             pass
            #         time.sleep(30)
            threads.close()
            
            time.sleep(10)
        


# @task
def main():
   # threads = ThreadingGroup(*host_list)
   # threads.run("sh /local/repository/installer.sh" )

    # Start the thread
    # recevier_thread = threading.Thread(target=dfreceiver)
    # recevier_thread.start()
    # Optionally, you can wait for the thread to complete
    runFabric()
    # recevier_thread.join()
main()
