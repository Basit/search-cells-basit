node=$(geni-get client_id)
time="$1"
echo "Time: $time"
echo "Node: $node"

if [ "$node" = "nuc1_law73" ]; then
    freq="2355e6"
elif [ "$node" = "nuc1_madsen" ]; then
    freq="763e6"
elif [ "$node" = "nuc1_ebc" ]; then
    freq="739e6"
elif [ "$node" = "nuc1_humanities" ]; then
    freq="763e6"
elif [ "$node" = "nuc1_sagepoint" ]; then
    freq="2165e6"
elif [ "$node" = "cellsdr1-bes-host" ]; then
    freq="1950e6"
elif [ "$node" = "cellsdr1-hospital-host" ]; then
    freq="1980e6"
elif [ "$node" = "cbrssdr1-fm-host" ]; then
    freq="2132.5e6"
else
    freq="0"
fi

echo "Frequency: $freq"


runFolder=/mydata/falconrun/$freq"_"$node"_"$time

sudo mkdir $runFolder

sudo /local/tools/falcon/build/src/FalconEye -D $runFolder/dci.csv -E $runFolder/stats.csv -r -N -G $runFolder/cellinfo.txt  -f $freq -n 300000  > $runFolder/logs.txt 2>&1
