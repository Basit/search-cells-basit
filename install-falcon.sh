#!/bin/bash

#
# Install Falcon
#

# Exit when any command fails
set -e

# Keep track of the last executed command
# trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG

# Echo an error message before exiting
# trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

PARENT="/local/tools"
SRSGUI="$PARENT"/srsGUI
FALCON="$PARENT"/falcon

sudo apt-get update

sudo apt-get -y install --no-install-recommends build-essential git subversion cmake libboost-system-dev libboost-test-dev libboost-thread-dev libfftw3-dev libsctp-dev libconfig-dev libconfig++-dev libmbedtls-dev
sudo apt-get -y install --no-install-recommends libboost-system-dev libboost-test-dev libboost-thread-dev libqwt-qt5-dev qtbase5-dev
sudo apt-get -y install --no-install-recommends libglib2.0-dev libudev-dev libcurl4-gnutls-dev libboost-all-dev qtdeclarative5-dev libqt5charts5-dev

mkdir -p $PARENT

# Install srsGUI
# if [ -d $SRSGUI ] 
# then
#     sudo rm -rf $SRSGUI
# fi
# cd $PARENT
# git clone --recursive https://github.com/srsLTE/srsGUI.git
# cd srsGUI
# mkdir build 
# cd build
# cmake ../
# make
# sudo make install
# sudo ldconfig

# Install Falcon
if [ -d $FALCON ] 
then
    sudo rm -rf $FALCON
fi
cd $PARENT
git clone --recursive https://gitlab.flux.utah.edu/Basit/falcon
cd falcon
mkdir build
cd build
cmake ../
host=$( cat /proc/sys/kernel/hostname )
if [[ $host == *"-host"* ]]; then
  sudo cp /local/repository/etc/simd.h /local/tools/falcon/build/srsLTE-src/lib/include/srslte/phy/utils/simd.h # Need this for X310
fi
make

exit 0