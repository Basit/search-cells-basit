"""Allocate a radio and run Falcon.
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
import geni.rspec.igext as ig
# Import the emulab extensions library.
import geni.rspec.emulab
# Import route for buses
import geni.rspec.emulab.route as route

def is_float(val):
  try:
    float(val)
    return True
  except ValueError:
    return False


#
# Setup the Tour info. We will add instructions below.
#  
tour = ig.Tour()
tour.Description(ig.Tour.TEXT, "Allocate a radio and run the monitor.");

IMAGE     = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-focal-image"
ENDPOINT  = "urn:publicid:IDN+cpg.powderwireless.net+authority+cm"
MS        = "urn:publicid:IDN+emulab.net+authority+cm"
COMMAND   = "/local/repository/monitor.pl"

#
# Two types of situations; B210 directly connected, and X310 ethernet connected.
#
fe_names = [
    ("web",
     "WEB"),
    ("guesthouse",
     "GuestHouse"),
    ("law73",
     "Law73"),
    ("ebc",
     "EBC"),
    ("humanities",
     "Humanities"),
    ("cpg",
     "CPG"),
    ("madsen",
     "Madsen"),
    ("sagepoint",
     "SagePoint"),
    ("moran",
     "Moran"),
     ("bookstore",
     "Bookstore")
]
portal.context.defineStructParameter(
    "radios", "Fixed Endpoints", [],
    multiValue=True,
    min=1,
    multiValueTitle="Fixed Endpoints.",
    members=[
        portal.Parameter(
            "radio_name",
            "Fixed Enpoints",
            portal.ParameterType.STRING,
            fe_names[0],
            fe_names)
    ])



radioTypes = [
    ('B210', 'B210'),
    ('X310', 'X310'),
]
# For X310s only.
computeTypes = [
    ('Any', 'Any'),
    ('d740', 'd740'),
    ('d430', 'd430'),
    ('d820', 'd820'),
]

# Create a portal context.
pc = portal.Context()

# Create a Request object to start building the RSpec. 
request = pc.makeRequestRSpec()

# pc.defineParameter("Where", "Where",
#                    portal.ParameterType.STRING, ENDPOINT)

pc.defineParameter("NodeID", "Node",
                   portal.ParameterType.STRING, "nuc1")

pc.defineParameter("Type", "Radio Type",
                   portal.ParameterType.STRING, radioTypes[0], radioTypes)

pc.defineParameter("ComputeType", "Compute Type",
                   portal.ParameterType.STRING, computeTypes[0], computeTypes,
                   longDescription="Select a type for X310 compute host")

# Store results to local www directory and start nginx
pc.defineParameter("WebSave", "Local Web Server",
                   portal.ParameterType.BOOLEAN, True,
                   longDescription="Save results to local directory and " +
                   "start a web server to access them. See the instructions " +
                   "for more information")

# Number of loops to run.
pc.defineParameter("RunCount", "Run Count", portal.ParameterType.INTEGER, 1,
                   longDescription="Number of times to run the monitor")

# Number of workers to use.
pc.defineParameter("Mode", "Mode", portal.ParameterType.INTEGER, 3,
                   longDescription="1 = cell_seach with band option, 2 = cell_search with frequency option, 3 = srsUE")

# Duration for one run.
pc.defineParameter("Duration", "Duration", portal.ParameterType.STRING, "5m",
                   longDescription="Duration for one run")

# Downlink freqeuncy to monitor.
pc.defineParameter("Channels", "Channels", portal.ParameterType.STRING, "731.5e6",
                   longDescription="A list of LTE DL channels to monitor.")

# Optional install only
pc.defineParameter("NoRun", "Install Only",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="Install but do not run the monitor")

params = pc.bindParameters()

# Check parameter validity.
# if params.Where == "":
#     pc.reportError(portal.ParameterError(
#         "You must provide an aggregate.", ["Where"]))
#     pass
if params.NodeID == "":
    pc.reportError(portal.ParameterError(
    "You must provide a node ID", ["NodeID"]))
    pass
if params.RunCount <= 0:
    pc.reportError(portal.ParameterError(
    "Run count must be greater the zero.", ["RunCount"]))
    pass
if params.Mode <= 0 or params.Mode > 3:
    pc.reportError(portal.ParameterError(
    "Mode must be 1, 2, or 3.", ["Mode"]))
    pass
if params.Channels == "":
    pc.reportError(portal.ParameterError(
    "A list of channels need to be specified.", ["Channels"]))
    pass

COMMAND += " -f " + params.Channels
COMMAND += " -l " + str(params.Duration)
COMMAND += " -m " + str(params.Mode)

if params.NoRun:
    COMMAND += " -n"
    pass
if params.WebSave:
    COMMAND += " -W"
    pass
if params.RunCount > 1:
    COMMAND += " -c " + str(params.RunCount)
    pass

nodes = {}

def startNode(radio):
    nodes[radio] = request.RawPC(params.NodeID+"_"+radio)
    nodes[radio].component_id         = params.NodeID 
    nodes[radio].component_manager_id =  "urn:publicid:IDN+"+radio+".powderwireless.net+authority+cm"
    nodes[radio].disk_image           = IMAGE
    nodes[radio].addService(pg.Execute(shell="sh",command="sudo sh /local/repository/installer.sh"))
    nodes[radio].addService(pg.Execute(shell="sh",command="sudo sh /local/repository/install-falcon.sh"))
    bs = nodes[radio].Blockstore("bs", "/mydata")
    bs.size = "200GB"
    
for i, radios in enumerate(params.radios):
    startNode(radios.radio_name)
#  

  #
  # Start up X11 VNC for display.
  #
    # node.startVNC()
  #
  # Run command
  #
    # node.addService(pg.Execute(shell="sh", command=COMMAND))

#
# Added instructions.
#

request.addTour(tour)
pc.verifyParameters()

# Final rspec.
pc.printRequestRSpec(request)
