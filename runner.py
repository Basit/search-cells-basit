import serial
import pandas as pd
from subprocess import run
import socket
output = run(['geni-get' , 'client_id'], capture_output=True).stdout.decode()
location = output.split("_")[1].split("\n")[0]
import sys
time = sys.argv[1]
tty = "/dev/ttyUSB2"
import uhd
import numpy as np

def getPower(center_freq):
    sample_rate = 10e6   # Sample rate in Hz
    gain = 38            # Gain in dB

    # Create a UHD USRP device
    usrp = uhd.usrp.MultiUSRP()

    # Set device parameters
    # rx_antenna = "RX2"  # Replace with the specific antenna you want to use
    usrp.set_rx_antenna("RX2", 0) 
    usrp.set_rx_rate(sample_rate, 0)
    usrp.set_rx_freq(uhd.libpyuhd.types.tune_request(center_freq), 0)
    usrp.set_rx_gain(gain, 0)

    # Set up the stream and receive buffer
    st_args = uhd.usrp.StreamArgs("fc32", "sc16")
    st_args.channels = [0]
    metadata = uhd.types.RXMetadata()
    streamer = usrp.get_rx_stream(st_args)
    recv_buffer = np.zeros((1, 1024), dtype=np.complex64)

    # Create a frequency vector for plotting
    stream_cmd = uhd.types.StreamCMD(uhd.types.StreamMode.start_cont)
    stream_cmd.stream_now = True
    streamer.issue_stream_cmd(stream_cmd)
    count = 0
    maxvalues = []
    try:
        while count<10:
            # Receive samples
            streamer.recv(recv_buffer, metadata)
            samples = recv_buffer[0]
            
            # Perform FFT and update the plot
            fft = np.abs(np.fft.fft(samples))**2 / (len(samples)*sample_rate)
            fft_log = 10.0*np.log10(fft)
            fft_result = np.fft.fftshift(fft_log)
            max_value = np.max(fft_result)  # Find the maximum value
            maxvalues.append(max_value)
            count+=1
        # fft_result = 10 * np.log10((np.abs(np.fft.fftshift(np.fft.fft(samples)** 2/len(samples))) ))

    except KeyboardInterrupt:
        # Stop the stream
        stream_cmd = uhd.types.StreamCMD(uhd.types.StreamMode.stop_cont)
        streamer.issue_stream_cmd(stream_cmd)

    # Close the UHD device
    # usrp.close()
    print("Max fft", sum(maxvalues)/len(maxvalues), location) 
    return sum(maxvalues)/len(maxvalues)


def get_lines_to_OK(serial_port):
    buffer_lines = []
    buffer = b""
    while True:
        buffer += serial_port.read(1)
        if len(buffer)==0:
            continue
        # print(buffer)
        if buffer[-1] in b'\n\r':
            buffer_lines.append(buffer)
            if buffer.strip() == b"OK":
                break
            buffer = b""

    return buffer_lines

with serial.Serial(tty, timeout=0.1) as serial_port:

    serial_port.reset_input_buffer()
    serial_port.write(b"AT\r")

    lines = get_lines_to_OK(serial_port)

    # serial_port.reset_input_buffer()
    # serial_port.write(b"AT+CFUN=1\r")

    # lines = get_lines_to_OK(serial_port)
    # print(lines)
    df = pd.DataFrame(columns=["Type","MCC", "MNC", "Freq", "PCI", "RSRP", "RSRQ", "srxlex", "squal/scs", "cellid", "TAC","Bandwidth","Band","Time","Location", "DLpower", "ULpower"])
    print("Started qscan at", location)

    serial_port.reset_input_buffer()
    serial_port.write(b'AT+QSCAN=2,1\n\r')
    lines = get_lines_to_OK(serial_port)
    # # print(lines)

    for line in lines:
        if line.startswith(b'+QSCAN: "LTE"'):
            line = line.decode('ascii')
            line = line.replace('\r','')
            parts = line.split(",")
            if(len(parts)>16):
                print("Malformed response", location)
                exit(1)
            if(len(parts)<10):
                print("Malformed response", location)
                exit(1)
            # 0 - LTE/5G
            # 1 - MCC
            # 2 - MNC
            # 3 - freq
            # 4 - PCI
            # 5 - RSRP
            # 6 - RSRQ
            # 7 - srxlex
            # 8 - squal
            # 9 - cellid
            # 10 - TAC

            entry = pd.DataFrame.from_dict({
                "Type": [parts[0].split("\"")[1]],
                "MCC": [parts[1]],
                "MNC":  [parts[2]],
                "Freq":  [parts[3]],
                "PCI":  [parts[4]],
                "RSRP":  [parts[5]],
                "RSRQ":  [parts[6]],
                "srxlex":  [parts[7]],
                "squal/scs":  [parts[8]],
                "cellid":  [parts[9]],
                "TAC":  [parts[10]],
                "Bandwidth":  [parts[11]],
                "Band":   [parts[12]],
                "Time": time,
                "Location": location,
                "DLpower": 0,
                "ULpower": 0

            })

            df = pd.concat([df, entry], ignore_index=True)
            mcc = parts[1]
            mnc = parts[2]
            cell_id = parts[9]
            pcid = parts[4]
            rsrp = parts[5]
            freq = parts[3]

        elif line.startswith(b'+QSCAN: "NR5G"'):
            line = line.decode('ascii')
            line = line.replace('\r','')
            parts = line.split(",")
            if(len(parts)>16):
                print("Malformed response", location)
                exit(1)
            if(len(parts)<10):
                print("Malformed response", location)
                exit(1)
            # 0 - LTE/5G
            # 1 - MCC
            # 2 - MNC
            # 3 - freq
            # 4 - PCI
            # 5 - RSRP
            # 6 - RSRQ
            # 7 - srxlex
            # 8 - scs
            # 9 - cellid
            # 10 - TAC
            # 11 - bandwidth
            # 12 - band

            entry = pd.DataFrame.from_dict({
                "Type": [parts[0].split("\"")[1]],
                "MCC": [parts[1]],
                "MNC":  [parts[2]],
                "Freq":  [parts[3]],
                "PCI":  [parts[4]],
                "RSRP":  [parts[5]],
                "RSRQ":  [parts[6]],
                "srxlex":  [parts[7]],
                "squal/scs":  [parts[8]],
                "cellid":  [parts[9]],
                "TAC":  [parts[10]],
                "Bandwidth":  [parts[11]],
                "Band":   [parts[12]],
                "Time": time,
                "Location": location,
                "DLpower": 0,
                "ULpower": 0

            })

            df = pd.concat([df, entry], ignore_index=True)
            mcc = parts[1]
            mnc = parts[2]
            cell_id = parts[9]
            pcid = parts[4]
            rsrp = parts[5]
            freq = parts[3]

    # downlink = getPower(2355e6)
    # uplink = getPower(2310e6)
    serial_port.reset_input_buffer()
    serial_port.write(b'AT+QSCAN=1,1\n\r')
    lines = get_lines_to_OK(serial_port)
    for line in lines:
        if line.startswith(b'+QSCAN: "LTE"'):
            line = line.decode('ascii')
            line = line.replace('\r','')
            parts = line.split(",")
            if(len(parts)>16):
                print("Malformed response", location)
                exit(1)
            if(len(parts)<10):
                print("Malformed response", location)
                exit(1)
            # 0 - LTE/5G
            # 1 - MCC
            # 2 - MNC
            # 3 - freq
            # 4 - PCI
            # 5 - RSRP
            # 6 - RSRQ
            # 7 - srxlex
            # 8 - squal
            # 9 - cellid
            # 10 - TAC
            entry = pd.DataFrame.from_dict({
                "Type": [parts[0].split("\"")[1]],
                "MCC": [parts[1]],
                "MNC":  [parts[2]],
                "Freq":  [parts[3]],
                "PCI":  [parts[4]],
                "RSRP":  [parts[5]],
                "RSRQ":  [parts[6]],
                "srxlex":  [parts[7]],
                "squal/scs":  [parts[8]],
                "cellid":  [parts[9]],
                "TAC":  [parts[10]],
                "Bandwidth":  [parts[11]],
                "Band":   [parts[12]],
                "Time": time,
                "Location": location,
                "DLpower": 0,
                "ULpower": 0

            })

            df = pd.concat([df, entry], ignore_index=True)
            mcc = parts[1]
            mnc = parts[2]
            cell_id = parts[9]
            pcid = parts[4]
            rsrp = parts[5]
            freq = parts[3]

        elif line.startswith(b'+QSCAN: "NR5G"'):
            line = line.decode('ascii')
            line = line.replace('\r','')
            parts = line.split(",")
            if(len(parts)>16):
                print("Malformed response", location)
                exit(1)
            if(len(parts)<10):
                print("Malformed response", location)
                exit(1)
            # 0 - LTE/5G
            # 1 - MCC
            # 2 - MNC
            # 3 - freq
            # 4 - PCI
            # 5 - RSRP
            # 6 - RSRQ
            # 7 - srxlex
            # 8 - scs
            # 9 - cellid
            # 10 - TAC
            # 11 - bandwidth
            # 12 - band

            entry = pd.DataFrame.from_dict({
                "Type": [parts[0].split("\"")[1]],
                "MCC": [parts[1]],
                "MNC":  [parts[2]],
                "Freq":  [parts[3]],
                "PCI":  [parts[4]],
                "RSRP":  [parts[5]],
                "RSRQ":  [parts[6]],
                "srxlex":  [parts[7]],
                "squal/scs":  [parts[8]],
                "cellid":  [parts[9]],
                "TAC":  [parts[10]],
                "Bandwidth":  [parts[11]],
                "Band":   [parts[12]],
                "Time": time,
                "Location": location,
                "DLpower": 0,
                "ULpower": 0

            })

            df = pd.concat([df, entry], ignore_index=True)
            mcc = parts[1]
            mnc = parts[2]
            cell_id = parts[9]
            pcid = parts[4]
            rsrp = parts[5]
            freq = parts[3]
    

    df.to_csv("/local/www/"+location + "_"+time + '.csv')

    # lines = get_lines_to_OK(serial_port)
    print("Wrote df to CSV at", location)
    df_csv = df.to_csv()


    host = '155.98.38.92'
    port = 34500
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    client_socket.connect((host, port))
    client_socket.send(df_csv.encode('utf-8'))
    client_socket.close()
