import serial
import pandas as pd
import time
from subprocess import run
import datetime

import sys
tty = "/dev/ttyUSB2"

ctime = str(datetime.datetime.now())
ctime = ctime.replace(' ', '_')
# dtm = dtm.replace(':', '/')
ctime = ctime.split('.')[0]

def location():
    ser = serial.Serial("/dev/ttyUSB1", timeout=0.1)

    while True:
        line = ser.readline().decode('utf-8').strip()
        if line.startswith("$GPGGA"):
            fields = line.split(',')
            print(line)

            fields = line.split(',')


            latitude_degrees = int(fields[2][:2])
            latitude_minutes = float(fields[2][2:])
            latitude = latitude_degrees + latitude_minutes / 60

            longitude_degrees = int(fields[4][:3])
            longitude_minutes = float(fields[4][3:])
            longitude = longitude_degrees + longitude_minutes / 60

            latitude_hemisphere = fields[3]
            longitude_hemisphere = fields[5]

            if latitude_hemisphere == 'S':
                latitude *= -1

            if longitude_hemisphere == 'W':
                longitude *= -1

            print("latitude: ", latitude)
            print("longitude: ", longitude)
            # time.sleep(10)
            return latitude,longitude
            # ser.close()

            # ser = serial.Serial(port, timeout=0.1)

def get_lines_to_OK(serial_port):
    buffer_lines = []
    buffer = b""
    while True:
        buffer += serial_port.read(1)
        if len(buffer)==0:
            continue
        #print(buffer)
        if buffer[-1] in b'\n\r':
            buffer_lines.append(buffer)
            if buffer.strip() == b"OK":
                break
            buffer = b""

    return buffer_lines

df_serve = pd.DataFrame(columns=["time","state","LTE","is_tdd","MCC","MNC","cellID","PCID","earfcn","freq_band_ind","UL_bandwidth","DL_bandwidth","TAC","RSRP","RSRQ","RSSI","SINR","CQI","tx_power","srxlev", "latitude", "longitude" ])

df = pd.DataFrame(columns=["Time","Type","MCC", "MNC", "Freq", "PCI", "RSRP", "RSRQ", "srxlex", "squal/scs", "cellid", "TAC","Bandwidth","Band", "latitude", "longitude"])

file_path = "logs/log_serve "+ ctime + ".txt"
logger= open(file_path, 'w')


def qscan(dtm, latitude, longitude):
    global df
    try:

        with serial.Serial(tty, timeout=0.1) as serial_port:

            serial_port.reset_input_buffer()
            serial_port.write(b"AT\r")

            lines = get_lines_to_OK(serial_port)

            # serial_port.reset_input_buffer()
            # serial_port.write(b"AT+CFUN=1\r")

            # lines = get_lines_to_OK(serial_port)
            # print(lines)

            serial_port.reset_input_buffer()
            serial_port.write(b'AT+QSCAN=1,1\n\r')
            lines = get_lines_to_OK(serial_port)
            for line in lines:
                if line.startswith(b'+QSCAN: "LTE"'):
                    line = line.decode('ascii')
                    parts = line.split(",")
                    if(len(parts)>16):
                        print("Malformed response", location)
                        exit(1)
                    if(len(parts)<10):
                        print("Malformed response", location)
                        exit(1)
                    # 0 - LTE/5G
                    # 1 - MCC
                    # 2 - MNC
                    # 3 - freq
                    # 4 - PCI
                    # 5 - RSRP
                    # 6 - RSRQ
                    # 7 - srxlex
                    # 8 - squal
                    # 9 - cellid
                    # 10 - TAC

                    entry = pd.DataFrame.from_dict({
                        "Type": [parts[0].split("\"")[1]],
                        "MCC": [parts[1]],
                        "MNC":  [parts[2]],
                        "Freq":  [parts[3]],
                        "PCI":  [parts[4]],
                        "RSRP":  [parts[5]],
                        "RSRQ":  [parts[6]],
                        "srxlex":  [parts[7]],
                        "squal/scs":  [parts[8]],
                        "cellid":  [parts[9]],
                        "TAC":  [parts[10]],
                        "Bandwidth":  " ",
                        "Band":  " ",
                        "Time": dtm,
                        "latitude": latitude,
                        "longitude": longitude

                    })
                    df = pd.concat([df, entry], ignore_index=True)
            df.to_csv("data/"+ "qscan_" + ctime + '.csv')

                
    except Exception as e:
        print(e)
        print("Failed for qscan at time: "+ str(dtm))



def serving(dtm, latitude,longitude):
    global df_serve
    try:

        with serial.Serial(tty, timeout=0.1) as serial_port:

            serial_port.reset_input_buffer()
            serial_port.write(b"AT\r")

            lines = get_lines_to_OK(serial_port)

            # serial_port.reset_input_buffer()
            # serial_port.write(b"AT+CFUN=1\r")

            # lines = get_lines_to_OK(serial_port)
            # print(lines)

            serial_port.reset_input_buffer()
            serial_port.write(b'AT+QENG="servingcell"\r')
            lines = get_lines_to_OK(serial_port)
            # print(lines)
            
            for line in lines:
                if line.startswith(b'+QENG:'):
                    logline = str(line) + "," + dtm +"\n"
                    print(logline)
                    logger.write(logline)
                    line = line.decode('ascii')
                    parts = line.split(",")
                    # if(len(parts)>16):
                    #     print("Malformed response", location)
                    #     exit(1)
                    # if(len(parts)<10):
                    #     print("Malformed response", location)
                    #     exit(1)
                    

                    entry = pd.DataFrame.from_dict({
                        "state": [parts[1]],
                        "LTE": [parts[2]],
                        "is_tdd":  [parts[3]],
                        "MCC":  [parts[4]],
                        "MNC":  [parts[5]],
                        "cellID":  [parts[6]],
                        "PCID":  [parts[7]],
                        "earfcn":  [parts[8]],
                        "freq_band_ind":  [parts[9]],
                        "UL_bandwidth":  [parts[10]],
                        "DL_bandwidth":  [parts[11]],
                        "TAC":  [parts[12]],
                        "RSRP":  [parts[13]],
                        "RSRQ":  [parts[14]],
                        "RSSI":  [parts[15]],
                        "SINR":  [parts[16]],
                        "CQI":  [parts[17]],
                        "tx_power":  [parts[18]],
                        "srxlev":  [parts[19].replace("/r","")],
                        "time": dtm,
                        "latitude": latitude,
                        "longitude": longitude
                        

                    })

                    df_serve = pd.concat([df_serve, entry], ignore_index=True)
            df_serve.to_csv("data/"+ "logs" + ctime + '.csv')
    except Exception as e:
        print(e)
        print("Failed for serving at time: "+ str(dtm))
    
while True:

    dtm = str(datetime.datetime.now())
    print("serving run: "+ dtm)
    latitude,longitude = location()
    serving(dtm,latitude,longitude)
    qscan(dtm,latitude,longitude)
    time.sleep(7)

