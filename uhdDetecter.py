import matplotlib

# Set the Matplotlib backend to Qt5 for interactive plotting
matplotlib.use("Qt5Agg")
import uhd
import numpy as np
import matplotlib.pyplot as plt

# UHD device configuration
center_freq = 2355e6  # Center frequency in Hz
sample_rate = 10e6   # Sample rate in Hz
gain = 38            # Gain in dB

# Create a UHD USRP device
usrp = uhd.usrp.MultiUSRP()

# Set device parameters
# rx_antenna = "RX2"  # Replace with the specific antenna you want to use
usrp.set_rx_antenna("RX2", 0) 
usrp.set_rx_rate(sample_rate, 0)
usrp.set_rx_freq(uhd.libpyuhd.types.tune_request(center_freq), 0)
usrp.set_rx_gain(gain, 0)

# Set up the stream and receive buffer
st_args = uhd.usrp.StreamArgs("fc32", "sc16")
st_args.channels = [0]
metadata = uhd.types.RXMetadata()
streamer = usrp.get_rx_stream(st_args)
recv_buffer = np.zeros((1, 1024), dtype=np.complex64)

# Create a frequency vector for plotting
freq_vector = np.fft.fftshift(np.fft.fftfreq(recv_buffer.shape[1], 1.0 / sample_rate))

# Initialize the plot
plt.ion()
fig, ax = plt.subplots()
line, = ax.plot(freq_vector / 1e6, np.zeros_like(freq_vector))

ax.set_title("UHD FFT Spectrum Analyzer")
ax.set_xlabel("Frequency (MHz)")
ax.set_ylabel("Amplitude (dB)")
ax.set_xlim(freq_vector[0] / 1e6, freq_vector[-1] / 1e6)
ax.set_ylim(-150, 10)
plt.grid()

# Start streaming and plotting
stream_cmd = uhd.types.StreamCMD(uhd.types.StreamMode.start_cont)
stream_cmd.stream_now = True
streamer.issue_stream_cmd(stream_cmd)

try:
    while True:
        # Receive samples
        streamer.recv(recv_buffer, metadata)
        samples = recv_buffer[0]
        
        # Perform FFT and update the plot
        fft = np.abs(np.fft.fft(samples))**2 / (len(samples)*sample_rate)
        fft_log = 10.0*np.log10(fft)
        fft_result = np.fft.fftshift(fft_log)
        max_value = np.max(fft_result)  # Find the maximum value
        print("Max value in FFT result:", max_value) 
        # fft_result = 10 * np.log10((np.abs(np.fft.fftshift(np.fft.fft(samples)** 2/len(samples))) ))
        line.set_ydata(fft_result)
        plt.pause(0.01)

except KeyboardInterrupt:
    # Stop the stream
    stream_cmd = uhd.types.StreamCMD(uhd.types.StreamMode.stop_cont)
    streamer.issue_stream_cmd(stream_cmd)

# Close the UHD device
usrp.close()